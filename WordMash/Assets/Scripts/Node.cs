﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node 
{
    public char thisChar;
    public bool visited;
    public List<Node> neighbours = new List<Node>();
    public int occurenceCount;
    public Vector2Int position;
    public Node()
    {
        thisChar = '\0';
        visited = false;
        position = new Vector2Int(-1, -1);

    }

    public Node(char c, int occurrence)
    {
        thisChar = c;
        visited = false;
        occurenceCount = occurrence;
        position = new Vector2Int(-1, -1);
    }

    /// <summary>
    /// Finds all the letters that are next to this letter in the random words selected
    /// </summary>
    /// <param name="words"></param>
    public void FindNeighbours(List<string> words)
    {
        foreach(string s in words)
        {
            for(int i = 0; i < s.Length; i++)
            {
                if(s[i] == thisChar)
                {
                    if (i > 0)
                    {
                        if (!neighbours.Exists(x => x.thisChar == s[i - 1]) && thisChar != s[i - 1])
                        {
                            neighbours.Add(GameManager.instance.ReturnNodeFromChar(s[i - 1]));
                        }
                    }
                        if (i < s.Length - 1)
                        {
                            if (!neighbours.Exists(x => x.thisChar == s[i + 1]) && thisChar != s[i + 1])
                            {
                                neighbours.Add(GameManager.instance.ReturnNodeFromChar(s[i + 1]));
                            }
                        }
                    
                }
            }
        }
    }

}
