﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    /*Algo for Generating alphabets
        * 1) Pick a random 5 letter and 4 letter words
        * 2) Check if other 4 letter or 3 letter words can be made by scrambling those letters
        * 3) If not, then go to step 1 else go to 4
        * 4) Find all the neighbours of all the nodes
        * 5) Place a letter in the first position and try to place the neighbours next to it recursively
        * */
    public WordGameDict wordGameDict;
    List<string> newWords = new List<string>();
    char[] nineChars = new char[9];
    static readonly int rows=3, cols = 3;

    List<List<Node>> gameMatrix = new List<List<Node>>();
    string st = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    Dictionary<char, int> occurenceCount = new Dictionary<char, int>();

    [HideInInspector]public List<Node> nodes = new List<Node>();
    public static GameManager instance = null;
    public bool isMouseDown = false;

    [HideInInspector]public string wordByUser = "";
    List<string> guessedWords = new List<string>();
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }
    void Start()
    {
        SetupInitialWords();
        foreach (string s in newWords)
        {
            Debug.Log(s);

        }

    }

    /// <summary>
    /// Called when player selects a new letter
    /// </summary>
    /// <param name="s"></param>
    public void NewLetterInput(string s)
    {
        wordByUser += s;
        UIManager.instance.UpdateWordText();
    }

    public void ResetInputWord()
    {
        ValidateUserWord();

        wordByUser = "";
        UIManager.instance.ResetButtons();
        UIManager.instance.UpdateWordText();
    }

    public void ValidateUserWord()
    {

        if(wordGameDict.CheckWord(wordByUser, 0) && !guessedWords.Exists(x=>x==wordByUser))
        {
            UIManager.instance.AddGuessedWord(wordByUser);
            guessedWords.Add(wordByUser);
        }
    }

    /// <summary>
    /// Gets 4 words witht the specifications provided
    /// </summary>
    public void SetupInitialWords()
    {
       

        newWords.Clear();
        nodes.Clear();
        nineChars = new char[9];
        gameMatrix.Clear();
        occurenceCount.Clear();

        newWords.Add(wordGameDict.RandomWord(5,5,newWords));

        for(int i = 0; i < 5; i++)
        {
            nineChars[i] = newWords[0][i];
        }
        char[] firstWord = newWords[0].ToCharArray();

        newWords.Add(wordGameDict.RandomWord(4,4, newWords));

        int counter = 4;
        for (int i = 0; i < 4; i++)
        {
            if (!Array.Exists(firstWord, x => x == newWords[1][i]))
            {
                counter++;
                nineChars[counter] = newWords[1][i];
                
            }
            else
            {
                int index = Array.FindIndex(firstWord, x => x == newWords[1][i]);
                if (index != -1)
                {
                    firstWord[index] = '\0';


                }
            }
        }

        if (counter < 8)
        {
            int numberOfRandomLettersToAdd = 8 - counter;

            for(int i = 0; i < numberOfRandomLettersToAdd; i++)
            {
                while (true)
                {
                    char c = st[UnityEngine.Random.Range(0, st.Length)];
                    if (Array.FindIndex(nineChars, x => x == c) == -1)
                    {
                        counter++;
                        nineChars[counter] = c;
                        break;
                    }
                }
            }
        }
      
        newWords.Add(wordGameDict.FindWord(nineChars,3, 3,newWords));
        newWords.Add(wordGameDict.FindWord(nineChars,3, 4,newWords));
        FillOccurenceHash();


        foreach(char c in nineChars)
        {
            nodes.Add(new Node(c, occurenceCount[c]));
        }

        foreach(Node n in nodes)
        {
            n.FindNeighbours(newWords);
        }

        SetupGameMatrix();
    }

    /// <summary>
    /// Fill the dictionary with character and count
    /// </summary>
    void FillOccurenceHash()
    {
        foreach (char c in nineChars)
        {
            if (!occurenceCount.ContainsKey(c))
            {
                occurenceCount.Add(c, 0);
            }
            else
            {
                occurenceCount[c] += 1;
            }
        }


        foreach(string s in newWords)
        {
            foreach(char c in s)
            {
                occurenceCount[c]++;
            }
        }

    }

    /// <summary>
    /// Initializes the 3x3 matrix with new nodes
    /// </summary>
    void SetupGameMatrix()
    {
        for(int r = 0; r < rows; r++)
        {
            gameMatrix.Add(new List<Node>());
            for(int c= 0; c < cols; c++)
            {
                gameMatrix[r].Add(new Node());
            }
        }

        foreach (Node n in nodes)
        {
            if (n.neighbours.Count != 0)
            {
                gameMatrix[0][0] = n;
                gameMatrix[0][0].position = new Vector2Int(0, 0);
                if (!FillGameMatrix(gameMatrix[0][0]))
                {
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            gameMatrix[i][j] = new Node();
                        }
                    }


                }
                else
                {
                    break;
                }


            }
        }

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (gameMatrix[i][j].thisChar=='\0')
                {
                    SetupInitialWords();
                    return;
                }
            }
        }

        UIManager.instance.UpdateButtonText(gameMatrix);
        for (int i = 0; i < 3; i++)
        {
            Debug.Log(gameMatrix[i][0].thisChar.ToString() + gameMatrix[i][1].thisChar + gameMatrix[i][2].thisChar);

        }
        UIManager.instance.FillHintText(newWords);
    }


   

    public Node ReturnNodeFromChar(char c)
    {
        return nodes.Find(x => x.thisChar == c);
    }

    /// <summary>
    /// Finds the positions next to the position provided
    /// </summary>
    /// <param name="row">Row</param>
    /// <param name="col">Column</param>
    /// <returns></returns>
    List<Vector2Int> FindSiblingPositions(int row, int col)
    {
        List<Vector2Int> pos = new List<Vector2Int>();
        if (row > 0)
        {
            pos.Add(new Vector2Int(row - 1, col));
        }
        if (row <rows-1)
        {
            pos.Add(new Vector2Int(row + 1, col));
        }
        if (col > 0)
        {
            pos.Add(new Vector2Int(row, col-1));
        }
        if (col < cols - 1)
        {
            pos.Add(new Vector2Int(row, col+1));
        }

        if(row >0 && col > 0)
        {
            pos.Add(new Vector2Int(row - 1, col-1));
        }
        if (row <rows-1 && col <cols-1)
        {
            pos.Add(new Vector2Int(row + 1, col + 1));
        }
        if(row>0 && col < cols-1)
        {
            pos.Add(new Vector2Int(row - 1, col + 1));
        }
        if (row < rows-1 && col >0)
        {
            pos.Add(new Vector2Int(row + 1, col - 1));
        }
        return pos;

    }

    /// <summary>
    /// Recursive function to put in the Letters in place
    /// </summary>
    /// <param name="currentNode"></param>
    /// <returns></returns>
    public bool FillGameMatrix(Node currentNode)
    {
        List<Vector2Int> siblingPos = FindSiblingPositions(currentNode.position.x,currentNode.position.y);
        foreach(Node n in currentNode.neighbours)
        {

            if(currentNode.neighbours.Count> siblingPos.Count)
            {
                return false;
            }
            bool shouldContinueToNextNeighbour = false;
            foreach(Vector2Int pos in siblingPos)
            {
                if (gameMatrix[pos.x][pos.y].thisChar == n.thisChar)
                {
                    shouldContinueToNextNeighbour = true;
                    break;
                }
            }
            if (shouldContinueToNextNeighbour)
            {
                continue;
            }
            bool neighbourPlacedSuccessfully = false;
            foreach(Vector2Int pos in siblingPos)
            {
                if(gameMatrix[pos.x][pos.y].thisChar == '\0')
                {
                    gameMatrix[pos.x][pos.y] = n;
                    n.position = pos;
                    if (!FillGameMatrix(n))
                    {
                        neighbourPlacedSuccessfully = false;
                        gameMatrix[pos.x][pos.y] = new Node();
                        n.position = new Vector2Int(-1, -1);
                    }
                    else
                    {
                        neighbourPlacedSuccessfully = true;
                        break;
                    }
                }
                
            }

            if (!neighbourPlacedSuccessfully)
            {
                return false;
            }
          
        }


        return true;


    }

}
