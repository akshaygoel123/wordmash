﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameManager.instance.isMouseDown = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            GameManager.instance.isMouseDown = false;
            GameManager.instance.ResetInputWord();

        }
    }

}

