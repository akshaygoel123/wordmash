﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class UIManager : MonoBehaviour
{
    public List<GameObject> buttons = new List<GameObject>();
    public static UIManager instance = null;
    public TextMeshProUGUI wordText;
    public TextMeshProUGUI hintText;
    public TextMeshProUGUI guessText;
    public GameObject horizontalConnector;
    public Transform canvas;
    public List<GameObject> instantiatedConnectors= new List<GameObject>();
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    /// <summary>
    /// Updates the letters on the buttons text
    /// </summary>
    /// <param name="nodes"></param>
    public void UpdateButtonText(List<List<Node>> nodes) 
    {
        int counter = 0;
        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 3; j++)
            {
                buttons[counter].GetComponent<CharacterHolder>().SetCharacter(nodes[i][j].thisChar.ToString());
                
                counter++;
            }
        }
    }

    /// <summary>
    /// Shows the current word made by user
    /// </summary>
    public void UpdateWordText()
    {
        wordText.text = GameManager.instance.wordByUser;
    }

    /// <summary>
    /// Resets the board after the user has made a guess
    /// </summary>
    public void ResetButtons()
    {
        foreach(GameObject g in buttons)
        {
            g.GetComponent<CharacterHolder>().ResetButton();
        }

        foreach(GameObject g in instantiatedConnectors)
        {
            Destroy(g);
        }
    }

    /// <summary>
    /// Adds text to the hints
    /// </summary>
    /// <param name="words"></param>
    public void FillHintText(List<string> words)
    {
        hintText.text = "The words are:\n";

        for(int i = 0; i < words.Count; i++)
        {
            hintText.text += words[i] + "\n";
        }
    }

    /// <summary>
    /// Button to toggle the hint text on and off
    /// </summary>
    public void ToggleHint()
    {
        if (hintText.gameObject.activeSelf)
        {
            hintText.gameObject.SetActive(false);
        }
        else
        {
            hintText.gameObject.SetActive(true);

        }
    }

    /// <summary>
    /// Restart the game
    /// </summary>
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Adds the correct guessed word to the list
    /// </summary>
    /// <param name="s"></param>
    public void AddGuessedWord(string s)
    {
        guessText.text += "\n" +s;
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// Shows a connector line as the player makes input
    /// </summary>
    /// <param name="pointA"></param>
    /// <param name="pointB"></param>
    public void DrawLine(Vector2 pointA, Vector2 pointB)
    {
        float lineWidth = 5;

        Vector3 differenceVector = pointB - pointA;
       GameObject line = Instantiate(horizontalConnector, canvas);
        instantiatedConnectors.Add(line);
        var imageRectTransform = line.GetComponent<RectTransform>();
        imageRectTransform.sizeDelta = new Vector2(differenceVector.magnitude, lineWidth);
        imageRectTransform.pivot = new Vector2(0f, 0.5f);
        imageRectTransform.localPosition = new Vector3(pointA.x, pointA.y, 0);
        float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
        line.transform.localRotation = Quaternion.Euler(0, 0, angle);
        line.transform.localPosition = new Vector3(line.transform.localPosition.x, line.transform.localPosition.y - 100f, 0);
    }
}
