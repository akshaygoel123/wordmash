﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class CharacterHolder : MonoBehaviour, IPointerEnterHandler,IPointerDownHandler
{
    string thisCharacter = "";
    public TextMeshProUGUI thisText;
    public bool isPressed;
    public Vector2Int position;

    static Vector2Int previousPosition = new Vector2Int(-1, -1);
    static Vector2 previousObjectPos = new Vector2(-1, -1);


    public string ThisCharacter
    {
        get { return thisCharacter; }
    }
    public void SetCharacter(string s)
    {
        thisText.text = s;
        thisCharacter = s;
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        if (GameManager.instance.isMouseDown && !isPressed)
        {
            Selected();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!isPressed)
        {
            Selected();
        }
    }

    /// <summary>
    /// This character is selected by player
    /// </summary>
    void Selected()
    {
        if ((previousPosition.x == -1 && previousPosition.y == -1) ||
               (Mathf.Abs(position.x - previousPosition.x) <= 1 && Mathf.Abs(position.y - previousPosition.y) <= 1))
        {
            GetComponent<Image>().color = Color.green;

            GameManager.instance.NewLetterInput(thisCharacter);
            isPressed = true;
            previousPosition = position;
            if (previousObjectPos.x != -1)
            {
                UIManager.instance.DrawLine(this.GetComponent<RectTransform>().localPosition, previousObjectPos);
            }
                previousObjectPos = this.GetComponent<RectTransform>().localPosition;
        }
    }

    /// <summary>
    /// Function called to reset the changes on this character button
    /// </summary>
    public void ResetButton()
    {
        isPressed = false;
        GetComponent<Image>().color = Color.white;
        previousPosition = new Vector2Int(-1, -1);
        previousObjectPos = new Vector2(-1, -1);
    }

}
