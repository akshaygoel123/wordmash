﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;
using System;
public class WordGameDict : MonoBehaviour{
	// In C# using a HashSet is an O(1) operation. It's a dictionary without the keys!
	private List<string> words = new List<string>();

	private TextAsset dictText;


    private void Awake()
    {
        InitializeDictionary("ospd");

    }

	public int CountOfWords()
    {
        return words.Count;
    }

	protected void InitializeDictionary(string filename){
		dictText = (TextAsset) Resources.Load(filename, typeof (TextAsset));
		var text = dictText.text;

		foreach (string s in text.Split('\n')){
            string newString = s.TrimEnd();
            newString = newString.ToUpper();
            words.Add(newString);
		}

        Debug.Log("Init Complete!");

	}



	
	public bool CheckWord(string word, int minLength){
		if (word.Length < minLength){
			return false;
		}

		return (words.Contains(word));
	}

    /// <summary>
    /// Finds a word that can be made from the list of characters provided
    /// </summary>
    public string FindWord(char[] charList, int minLen, int maxLen, List<string> previousWords)
    {
        foreach(string s in words)
        {
            bool flag = true;
            if (s.Length >= minLen && s.Length <= maxLen && !previousWords.Contains(s) &&  uniqueCharacters(s)) {

                char[] sChars = s.ToCharArray();
                char[] charCopy = new char[9];

                for(int i = 0; i < 9; i++)
                {
                    charCopy[i] = charList[i];
                }
                foreach(char c in sChars)
                {
                    if (!Array.Exists(charCopy, x => x == c))
                    {
                        flag = false;
                        break;
                    }
                    else
                    {
                        int index = Array.FindIndex(charCopy, x => x == c);
                        if(index!=-1)
                        charCopy[index] = '\0';
                    }
                }
                if (flag)
                {
                    return s;
                }
            }
        }
        return null;
    }


    /// <summary>
    /// Finds a Random word from the dictionary provided
    /// </summary>
    /// <param name="minLength"></param>
    /// <param name="maxLength"></param>
    /// <param name="previousWords"></param>
    /// <returns></returns>
    public string RandomWord(int minLength, int maxLength, List<string> previousWords)
    {
        int i = 0;
        while (i<1000)
        {
            
            int rand = UnityEngine.Random.Range(0, words.Count);

            if (words[rand].Length >= minLength && words[rand].Length <= maxLength && !previousWords.Contains(words[rand]) && uniqueCharacters(words[rand]))
            {
                return words[rand];
            }

            i++;
        }
        Debug.LogError("Please make sure the dictionary is implemented properly.");
        throw new System.ArgumentOutOfRangeException("words");
    }

    /// <summary>
    /// Returns true if none of the characters are repeated
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public virtual bool uniqueCharacters(string str)
    {
        // Assuming string can have 
        // characters a-z this has 
        // 32 bits set to 0 
        int checker = 0;

        for (int i = 0; i < str.Length; i++)
        {
            int bitAtIndex = str[i] - 'a';

            // if that bit is already set 
            // in checker, return false 
            if ((checker & (1 << bitAtIndex)) > 0)
            {
                return false;
            }

            // otherwise update and continue by 
            // setting that bit in the checker 
            checker = checker | (1 << bitAtIndex);
        }

        // no duplicates encountered, 
        // return true 
        return true;
    }
}
